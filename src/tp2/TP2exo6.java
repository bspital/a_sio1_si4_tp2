package tp2;

import java.util.Scanner;

public class TP2exo6 {

    public static void main(String[] args) {
        float taille, poidsidealL, poidsidealC = 0;
        int age, morphologie;
        String sexe;
        
        Scanner saisie = new Scanner(System.in);
        System.out.println("Quelle est votre taille en cm ?");
        taille = saisie.nextFloat();
        System.out.println("Quelle est votre age ?");
        age = saisie.nextInt();
        System.out.println("Quelle est votre sexe (F ou M) ?");
        sexe = saisie.next();
        System.out.println("Quelle est votre morphologie (1 = fine; 2 = normale; 3 = large)  ?");
        morphologie = saisie.nextInt();
        
        if(sexe.equals("F")) {
            poidsidealL = (float) (taille - 100 - (taille-150) / 2.5);
        }
        else {
            poidsidealL = (float) (taille - 100 - (taille-150) / 4);
        }
        
        if(morphologie == 1) {
            poidsidealC = (float) ((taille-100+age/10)*0.9);
        }
        else if(morphologie == 2) {
            poidsidealC = (float) ((taille-100+age/10)*0.9*0.9);
        }
        else if(morphologie == 3) {
            poidsidealC = (float) ((taille-100+age/10)*0.9*1.1);
        }
        
        System.out.println("Votre poids ideal selon la formule de Lorentz est de: " + poidsidealL);
        System.out.println("Votre poids ideal selon la formule de Creff est de: " + poidsidealC);
    }
}
