package tp2;

import java.util.Scanner;

public class TP2exo4 {

    public static void main(String[] args) {
        Scanner saisie = new Scanner(System.in);
        float ca, com;
        System.out.println("Veuillez saisir le chiffre d'affaires que vous avez realise en tant que representant.");
        ca = saisie.nextFloat();
        if(ca < 10000) {
            com = (float) (ca * 0.02);
        }
        else if(ca >= 10000 && ca < 20000){
            com = (float) (200 + (ca - 10000) * 0.04);
        }
        else {
            com = (float) (600 + (ca - 20000) * 0.06);
        }
        System.out.println("Vous recevrez une commission de " + com + " euros.");
    }
}
