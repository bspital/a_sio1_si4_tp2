package tp2;

import java.util.Scanner;

public class TP2exo1 {

    public static void main(String[] args) {
        Scanner saisie = new Scanner(System.in);
        
        float longueur, largeur, perim, surface;
        
        System.out.println("Quelle est la longueur du rectangle en cm ?");
        longueur = saisie.nextFloat();
        System.out.println("Quelle est la largeur du rectangle en cm ?");
        largeur = saisie.nextFloat();
        
        perim = longueur*2+largeur*2;
        surface = longueur*largeur;
        System.out.println("Le rectangle a un perimetre de " + perim + "cm et une surface de " + surface + "cm^2.");
    }
}
